\documentclass{article}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[utf8]{inputenc}
\usepackage[russian]{babel}

% % Triple dots redefinition
% % According to https://tex.stackexchange.com/questions/131587/mathtools-dddot-cause-the-term-to-rise/131591#131591
% \DeclareFontFamily{U}{mathb}{\hyphenchar\font45}
% \DeclareFontShape{U}{mathb}{m}{n}{
%       <5> <6> <7> <8> <9> <10> gen * mathb
%       <10.95> mathb10 <12> <14.4> <17.28> <20.74> <24.88> mathb12
%       }{}
% \DeclareSymbolFont{mathb}{U}{mathb}{m}{n}
% \DeclareFontSubstitution{U}{mathb}{m}{n}
% \let\dddot\relax
% \DeclareMathAccent{\dddot}{0}{mathb}{"3B}

\begin{document}
\begin{center}
\Large{Кривые в евклидовом пространстве}
\end{center}

Базис Френе и формулы Френе для кривых в пространствах размерности $n = 2$.

Мы собираемся сопоставить каждой точке произвольной плоской кривой сопоставить определённый ортонормированный базис, отражающий геометрию кривой в окрестности этой точки и вывести формулы, связывающие векторы этого базиса и их производные. Этот базис мы будем называть \textit{базисом Френе}, а формулы --- \textit{формулами Френе}.

Рассмотрим натурально параметризованную кривую $r(s)$. Тогда длина вектора скорости $|v(s)| = |\dot r(s)| = 1$. Рассмотрим вектор $\ddot r(s) = \dot v(s)$. Оказывается, что $\ddot r(s) \perp v(s)$ и чтобы показать это, докажем сперва следующую несложную лемму.

Лемма. Для любой вектор-функции $a(s)$, такой, что $|a(s)| = 1$ $\forall s$ выполнено $a(s) \perp \dot a(s)$ для любого значения параметра $s$. Доказательство\footnote{Замечание касательно обозначений в смысле пропуска явного указания параметра}. $\langle a, a\rangle = 1 \Rightarrow \frac{d}{dt}\langle a, a\rangle = 0 \Rightarrow \langle \dot a, a\rangle + \langle a, \dot a\rangle = 0 \Rightarrow \langle a, \dot a\rangle = 0 \Rightarrow a \perp \dot a$, что и требовалось.

Подставив в условие леммы $a = v = \dot r$ и $\dot a = \dot v = \ddot r$ получим сформулированное выше утверждение. Таким образом, векторы $v$ и $\ddot r$ образуют ортогональный базис. Полученный базис уже ортогональный, но пока ещё не ортонормированный. Так как мы знаем, что длина его первого вектора $|v| = 1$, то нам осталось только поделить второй вектор на его длину $|\ddot r|$. А это, как мы уже знаем, в точности кривизна $k$ рассматриваемой плоской кривой. Таким образом, второй вектор базиса имеет вид $n = \frac{\ddot r}{|\ddot r|} = \frac{\ddot r}{k} = \frac{\dot v}{k}$. Он называется \textit{нормалью} или \textit{нормальным вектором} и в совокупности с вектором скорости образует ортонормированный базис $\{v, n\}$, который и называется \textit{базисом Френе}.

Небольшое терминологическое замечание касательно базисов и реперов\footnote{Хохма про физиков и математиков рисующих нормальные векторы в разные стороны}.

Теперь выведем формулы, выражающие $\dot v$ и $\dot n$ через $v$ и $n$. Для $\dot v$ это очень просто. Вспомним определение нормали $n = \frac{\dot v}{k}$ и домножим левую и правую части равенства на $k$. Получим $\dot v = kn$. Вычислить $\dot n$ немного сложнее. Как мы знаем, $|n| = 1$, а значит, по доказанной выше лемме, $\dot n \perp n$. Ввиду того, что размерность объемлющего пространства равна 2 и $v \perp n$, выполняется $\dot n \parallel v \Rightarrow \dot n = \lambda v$ для некоторого $\lambda$. $\langle v, n\rangle = 0 \Rightarrow \langle \dot v, n\rangle + \langle v, \dot n\rangle = 0 \Rightarrow \langle kn, n\rangle + \langle v, \lambda v\rangle = 0 \Rightarrow k + \lambda = 0 \Rightarrow \lambda = -k$. А значит $\dot n = \lambda v = -kv$.

Таким образом, мы получили следующие равнства, называемые \textit{формулами Френе}.

$$
  \left\{ 
    \begin{aligned}
      & \dot v(s) = k(s)n(s) \\
      & \dot n(s) = -k(s)v(s) \\
    \end{aligned}
  \right.
$$

Замечание о геометрическом смысле формул. Рассмотрим на кривой $r$ близкие точки $r(s)$ и $r(s + \varepsilon)$. Им соответствуют (ортонормированные) базисы Френе $\{v(s), n(s)\}$ и $\{v(s + \varepsilon), n(s + \varepsilon)\}$. Так как кривая гладкая, а расстояние между точками мало, базисы ориентированы одинаково. Попробуем понять, как устроен оператор поворота, для чего воспользуемся только что полученными формулами Френе. Перепишем их в следующем виде:

$$
  \begin{aligned}
    v(s + \varepsilon) & = v(s) + \varepsilon k(s)n(s) + o(\varepsilon) \\
    n(s + \varepsilon) & = n(s) - \varepsilon k(s)v(s) + o(\varepsilon).
  \end{aligned}
$$

Вспомнив известные из курса элементарного анализа формулы $\sin(x) = x + o(x)$ и $\cos(x) = 1 + o(x)$, приведём их к виду:

$$
  \begin{aligned}
    v(s + \varepsilon) & = \cos(\varepsilon k(s)) v(s) + \sin(\varepsilon k(s))n(s) + o(\varepsilon) \\
    n(s + \varepsilon) & = -\sin(\varepsilon k(s))v(s) + \cos(\varepsilon k(s)) n(s) + o(\varepsilon).
  \end{aligned}
$$

То есть с точностью до $o(\varepsilon)$ это поворот на $\varepsilon k(s)$ радиан.

Кривизна в терминах вращения касательно кривой. $k(s) = \lim_{\varepsilon \to \infty}\frac{\alpha(k, \varepsilon)}{\varepsilon}$.

Базис Френе и формулы Френе для кривых в пространствах размерности $n = 3$.

Регулярность кривой аналогична плоскому случаю. Натуральный параметр аналогично. Кривизна $k = |\ddot r|$ при натуральной параметризации тоже аналогично. Векторы $v = \dot r$ и $n = \frac{\dot v}{k}$ тоже аналогично. Третий вектор для базиса Френе легко получается путём применения векторного произведения к уже имеющимся двум, $b = [v, n]$. Вектор $n$ называется \textit{главным нормальным вектором} или \textit{главной нормалью}, а вектор $b$ называется \textit{бинормальным}.

Продолжая аналогию с двумерным случаем, займёмся выводом формул Френе. По соображениям, полностью повторяющим плоский случай, $\dot v = kn$. Для вычисления $\dot n$ проведём следующую цепочку рассуждений: $|n| = 1 \Rightarrow \dot n \perp n \Rightarrow \dot n = \lambda v + \mu b$. Снова вспоминая уже проведённое в двумерном случае рассуждение, $\langle n, v\rangle = 0 \Rightarrow \langle \dot n, v\rangle + \langle n, \dot v\rangle = 0 \Rightarrow \langle \lambda v + \mu b, v\rangle + \langle n, kn\rangle = 0 \Rightarrow \lambda + k = 0 \Rightarrow \lambda = -k$. А вот коэффициент $\mu$ выразить, как оказывается, нельзя. Обозначают его обычно через $\varkappa$ и называют \textit{кручением}. Чуть более точно:

Определение: кручением натурально параметризованной трёхмерной кривой называют величину $\varkappa = \langle \dot n, b \rangle$.

Таким образом, можно записать $\dot n = -kv + \varkappa b$.

Не имеющее прямого аналога в уже рассмотренном случае размерности два вычисление произвольной бинормали ... $\dot b = \frac{d}{ds}[v, n] = [\dot v, n] + [v, \dot n] = [kn, n] + [v, kv + \varkappa b] = \varkappa [v, b] = -\varkappa n$.

Итак, формулы Френе для трёхмерных кривых:

$$
  \left\{ 
    \begin{aligned}
      & \dot v(s) = k(s)n(s) \\
      & \dot n(s) = -k(s)v(s) + \varkappa b \\
      & \dot b(s) = -\varkappa n.
    \end{aligned}
  \right.
$$

Базис Френе и формулы Френе для кривых в пространствах размерности $n > 3$.

Запишем формулы Френе в матричном виде.

$$
\begin{pmatrix}
\dot v \\
\dot n \\
\dot b \\
\end{pmatrix}
=
\begin{pmatrix}
 0 &          k &         0 \\
-k &          0 & \varkappa \\
 0 & -\varkappa &         0 \\
\end{pmatrix}
\begin{pmatrix}
v \\
n \\
b \\
\end{pmatrix}
$$

Что можно сказать, глядя на эту матрицу? Она кососимметрическая, более того, её ненулевые элементы лежат только на наддиагонали и поддиагонали (терминология? --- А. Г.). Попробуем, с учётом этого замечания, перенести проведённые выше рассуждения для кривых в $\mathbb{E}^2$ и $\mathbb{E}^3$ на случай кривых в евклидовых пространствах более высоких размерностей $\mathbb{E}^n, n > 3$.

К сожалению, в случае пространства произвольной размерности, подходящего для нашей цели аналога векторного произведения нет, и напрямую перенести конструкцию репера Френе со случая $n = 3$ не удастся. Однако оказываеться, что можно использовать другой, более универсальный, подход, использующий процесс ортогонализации по Грамму-Шмидту. Рассмотрим кривую $r : [a, b] \rightarrow \mathbb{E}^n$. Для того, чтобы получить базис в $n$-мерном пространстве нам потребуется набор из $n$ векторов. Рассмотрим векторы $v = \dot r, \ddot r, \ldots, r^{(n)}$, ортогонализуем их по Грамму-Шмидту и отнормируем результат. Вспомним, что в случаях размерностей 2 и 3, то репер Френе мог оказаться не определён, например если кривизна равно нулю (проверить что там в каждом из случаев подробнее - А. Г.). Так что для произвольного набора векторов в произвольном $n$-мерном проделать аналогичную операцию тоже не получится потому что они могут быть линейно зависимы.

(описание трюка с выбором последнего вектора для <<правильной>> ориентации репера)
небольшую хитрость... Допустим, что мы уже ортонормировали $n - 1$ векторов. Выбрать последний так, чтобы он дополнял до положительно ориентированного базиса.

(замечание о соответствии построенной конструкции трёхмерному случаю и особенности двумерного)

Для обобщения формул Френе на многомерный случай начнём со следующего важного наблюдения. Первый вектор нового, ортонормированного базиса выражается через первый вектор старого. Второй вектор нового базиса выражается через первый и второй векторы старого. Наконец, $n$-ный (n-1 ый?) --- через первые $n$ векторов старого.

$$
\begin{aligned}
& g_1 = f_1(\dot r) \\
& g_2 = f_2(\dot r, \ddot r) \\
& \cdots \\
& g_i = f_i(\dot r, \ldots, r^{(i)})
\end{aligned}
$$

Мы знаем, что эта зависимость линейная, то есть все $f_i$ --- линейные функции (переписать формулы выше сразу в линейной форме? --- А. Г.). Это значит, что в матричной форме она будет иметь следующий вид:

$$
G = F R
$$

$$
\begin{aligned}
& \dot r = h_1(g_1) \\
& \ddot r = h_2(g_1, g_2) \\
& \cdots \\
& r^{(i)} = h_i(g_1, g_2, \ldots, g_i)
\end{aligned}
$$

Как видим, матрица $F$ треугольная (и ещё надо понять, почему невырожденная). Так что её можно обратить и обратная к ней $F^{-1}$ тоже будет треугольной.

$$
R = F^{-1} G
$$

Обратим теперь внимание на следующее соображение.

$$
\begin{aligned}
& \dot g_1 = F_1(\dot r, \ddot r) = G_1(g_1, g_2) \\
& \dot g_2 = F_2(\dot r, \ddot r, r^{(3)}) = G_2(g_1, g_2, g_3) \\
& \cdots \\
\end{aligned}
$$

Это значит, что матрица $B(s)$ выглядит следующим образом:

$$
B(s) = 
\begin{pmatrix}
* & * & 0 & 0 & \ldots & 0 & 0 \\
* & * & * & 0 & \ldots & 0 & 0 \\
* & * & * & * & \ldots & 0 & 0 \\
* & * & * & * & \ldots & 0 & 0 \\
\vdots & \vdots & \vdots & \vdots & \vdots & \ddots & \vdots \\
* & * & * & * & \ldots & * & * \\
* & * & * & * & \ldots & * & * \\
\end{pmatrix}
$$

С учётом её кососимметричности, получим, что 

$$
B(s) = 
\begin{pmatrix}
   0 &  k_1 & 0 &   0 & \ldots & 0 & 0 \\
-k_1 &    0 & k_2 & 0 & \ldots & 0 & 0 \\
   0 & -k_2 & 0 & k_3 & \ldots & 0 & 0 \\
0 & 0 & -k_3 & 0      & \ldots & 0 & 0 \\
\vdots & \vdots & \vdots & \vdots & \ddots & \vdots & \vdots \\
0 & 0 & 0 & 0 & \ldots & 0 & k_{n-1}\\
0 & 0 & 0 & 0 & \ldots & -k_{n-1} & 0 \\
\end{pmatrix}
$$

для некоторых $k_i \in \mathbb{R}$.

=== \\

\begin{center}
\Large{Поверхности в евклидовом пространстве}
\end{center}

Рассмотрим поверхность $\Sigma$ вложенную в $\mathbb{E}^3$, точку $A$ на ней и касательное пространство $T_A\Sigma$. Рассмотрим также натурально параметризованную кривую $r$, лежащую на поверхности и проходящую через точку $A$. Пусть вектор $\vec v$ [$\dot r$(0)?] --- касательный вектор к кривой, лежит в касательной плоскости $T_A\Sigma$. Рассмотрим образующие (чего? --- А. Г.) при размерности поверхности $k = 2$ и размерности объемлющего пространства $n = 3$. Касательное пространство к друмерной поверхности тоже имеет размерность 2, а нормальное пространство --- размерность $n - k = 3 - 2 = 1$, то есть это просто одномерное нормальное направление. Пусть вектор $\vec m$ --- это нормаль к $\Sigma$. Рассмотрим главную нормаль к кривой $\vec n$ в точке $A$. Нетрудно видеть, что, вообще говоря, она никак не связана с нормалью к поверхности. Вектор $\ddot r$ пропорционален $\vec n$, а длина его равна кривизне $\varkappa$.

Вернемся к рассмотрению поверхности $\Sigma$ и точки $A$. Пространство распадается в сумму касательного $T_A\Sigma$ и нормального $N_A\Sigma$. Разложим вектор $\ddot r$ в сумму проекций на касательное и нормальное пространства. Длина проекции на нормальный вектор $\varkappa_n$ называется нормальной кривизной, а на касательный $\varkappa_g$ --- геодезической кривизной. По известной из школьного курса теореме Пифагора $\varkappa^2 = \varkappa_n^2 + \varkappa_g^2$. Оказывается, что нормальная и геодезическая кривизны имеют совершенно разные свойства и изучать их будем по отдельности. Продолжим в следующий раз.


\end{document}
